package net.ambtec.keycloakeventservice;

import org.springframework.data.jpa.repository.JpaRepository;

public interface KeycloakEventRepository extends JpaRepository<KeycloakEvent, String> {
}
