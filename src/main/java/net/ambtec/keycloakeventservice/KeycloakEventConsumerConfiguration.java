package net.ambtec.keycloakeventservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.keycloak.events.Event;
import org.keycloak.events.admin.AdminEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.function.Function;

@Configuration
@Log4j2
@RequiredArgsConstructor
public class KeycloakEventConsumerConfiguration {

    private final KeycloakEventRepository keycloakEventRepository;

    @Bean
    public ObjectMapper objectMapperObjectProvider() {
        var mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        return mapper;
    }

    @Bean
    public Function<Event, IAMEvent> keycloakUserEvents(ObjectMapper objectMapper) {
        return keycloakEvent -> {
            try {
                String payload = objectMapper.writeValueAsString(keycloakEvent);
                log.info("user event received: {}", payload);
                KeycloakEvent internalKeycloakEvent = saveEvent(keycloakEvent, payload);
                return mapTo(internalKeycloakEvent);
            } catch (JsonProcessingException e) {
                log.error("error serializing user event in json", e);
            }

            return null;
        };
    }

    @Bean
    public Function<AdminEvent, IAMEvent> keycloakAdminEvents(ObjectMapper objectMapper) {
        return keycloakEvent -> {
            try {
                String payload = objectMapper.writeValueAsString(keycloakEvent);
                log.info("admin event received: {}", payload);
                KeycloakEvent internalKeycloakEvent = saveEvent(keycloakEvent, payload);
                return mapTo(internalKeycloakEvent);
            } catch (JsonProcessingException e) {
                log.error("error serializing admin event in json", e);
            }

            return null;
        };
    }

    private KeycloakEvent saveEvent(AdminEvent adminEvent, String payload) {
        return keycloakEventRepository.save(
                KeycloakEvent.builder()
                        .eventId(adminEvent.getId())
                        .eventType(KeycloakEvent.KeycloakEventType.ADMIN)
                        .eventAction(adminEvent.getOperationType() + " " + adminEvent.getResourceType())
                        .realmId(adminEvent.getRealmId())
                        .date(map(adminEvent.getTime()))
                        .userId(adminEvent.getAuthDetails().getUserId())
                        .clientId(adminEvent.getAuthDetails().getClientId())
                        .payload(payload)
                        .build()
        );
    }

    private KeycloakEvent saveEvent(Event keycloakEvent, String payload) {
        return keycloakEventRepository.save(
                KeycloakEvent.builder()
                        .eventId(keycloakEvent.getId())
                        .eventType(KeycloakEvent.KeycloakEventType.USER)
                        .eventAction(keycloakEvent.getType().toString())
                        .realmId(keycloakEvent.getRealmId())
                        .date(map(keycloakEvent.getTime()))
                        .userId(keycloakEvent.getUserId())
                        .clientId(keycloakEvent.getClientId())
                        .payload(payload)
                        .build()
        );
    }

    private IAMEvent mapTo(KeycloakEvent keycloakEvent) {
        return new IAMEvent(keycloakEvent.getEventId(),
                keycloakEvent.getRealmId(),
                keycloakEvent.getEventAction(),
                keycloakEvent.getDate(),
                keycloakEvent.getUserId(),
                keycloakEvent.getClientId());
    }

    private LocalDateTime map(long time) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneOffset.UTC);
    }
}

