package net.ambtec.keycloakeventservice;

import java.time.LocalDateTime;

public record IAMEvent(String id, String realm, String event,
                       LocalDateTime date, String userId, String clientId) {
}
