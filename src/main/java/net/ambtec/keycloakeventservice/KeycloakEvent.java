package net.ambtec.keycloakeventservice;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class KeycloakEvent {

    @Id
    private String eventId;
    @Enumerated(EnumType.STRING)
    private KeycloakEventType eventType;
    private String eventAction;
    private String realmId;
    private LocalDateTime date;
    private String userId;
    private String clientId;
    @Column(length = 2000)
    private String payload;

    public enum KeycloakEventType {
        USER, ADMIN
    }
}
