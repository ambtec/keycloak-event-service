package net.ambtec.keycloakeventservice;

import lombok.extern.java.Log;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Log
public class KeycloakEventServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(KeycloakEventServiceApplication.class, args);
    }
}
