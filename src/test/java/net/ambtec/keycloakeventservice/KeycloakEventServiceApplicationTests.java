package net.ambtec.keycloakeventservice;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.stream.binder.test.InputDestination;
import org.springframework.cloud.stream.binder.test.OutputDestination;
import org.springframework.cloud.stream.binder.test.TestChannelBinderConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

@Log4j2
class KeycloakEventServiceApplicationTests {

    @Test
    void process_adminEvent() {
        try (ConfigurableApplicationContext context = new SpringApplicationBuilder(
                TestChannelBinderConfiguration.getCompleteConfiguration(
                        KeycloakEventServiceApplication.class))
                .profiles("test")
                .run()) {
            InputDestination inputDestination = context.getBean(InputDestination.class);
            OutputDestination outputDestination = context.getBean(OutputDestination.class);


            Message<String> inputMessage = MessageBuilder.withPayload(loadTestFile("admin-event-create-client.json")).build();
            inputDestination.send(inputMessage, "iam-admin-events");

            Message<byte[]> outputMessage = outputDestination.receive(0, "iam-events");
            assertMessage(outputMessage, "admin-event-create-client_expected.json");
        }
    }

    @Test
    void process_userEvent() {
        try (ConfigurableApplicationContext context = new SpringApplicationBuilder(
                TestChannelBinderConfiguration.getCompleteConfiguration(
                        KeycloakEventServiceApplication.class))
                .profiles("test")
                .run()) {
            InputDestination inputDestination = context.getBean(InputDestination.class);
            OutputDestination outputDestination = context.getBean(OutputDestination.class);


            Message<String> inputMessage = MessageBuilder.withPayload(loadTestFile("user-event-login.json")).build();
            inputDestination.send(inputMessage, "iam-user-events");

            Message<byte[]> outputMessage = outputDestination.receive(0, "iam-events");
            assertMessage(outputMessage, "user-event-login_expected.json");
        }
    }

    @SneakyThrows
    private void assertMessage(Message<byte[]> outputMessage, String expectedFilename) {
        String actual = new String(outputMessage.getPayload(), StandardCharsets.UTF_8);
        log.info("actual response: {}", actual);
        JSONAssert.assertEquals(loadTestFile(expectedFilename), actual, false);
    }

    @SneakyThrows
    private String loadTestFile(String filename) {
        InputStream stream = this.getClass().getResourceAsStream(filename);
        String fileContent = new String(Objects.requireNonNull(stream).readAllBytes(), StandardCharsets.UTF_8);
        log.info("loaded file content: {}", fileContent);
        return fileContent;
    }
}
