#syntax=docker/dockerfile:experimental

FROM maven:3-openjdk-17-slim as builder

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN --mount=type=cache,target=/root/.m2 mvn package

FROM openjdk:17-slim

COPY --from=builder /usr/src/app/target/*.jar /app.jar

EXPOSE 8080

ENTRYPOINT ["java"]
CMD ["-jar", "/app.jar"]
